return {
  "numToStr/FTerm.nvim",
  keys = { "<A-i>", "<A-z>", "<A-n>", "<A-p>", "<A-t>", "<A-g>" },
  config = function()
    local fterm = require("FTerm")
    fterm.setup({
      border = "rounded",
      hl = "NormalFloat",
      blend = 0,
    })

    local map = vim.keymap.set
    local fn = vim.fn
    local opts = { silent = true }
    local runners = { python = 'python' }

    map('n', '<leader><Enter>', function()
      local buf = vim.api.nvim_buf_get_name(0)
      local ftype = vim.filetype.match({ filename = buf })
      local exec = runners[ftype]
      if exec ~= nil then
        fterm.scratch({ cmd = { exec, buf } })
      end
    end)

    -- primary terminal
    map("n", "<A-i>", "<CMD>lua require('FTerm').toggle()<CR>", opts)
    map("t", "<A-i>", "<C-\\><C-n><CMD>lua require('FTerm').toggle()<CR>", opts)
    map("t", "<A-c>", "<C-\\><C-n><CMD>lua require('FTerm').exit()<CR>", opts)

    local zola_serve = fterm:new({
      ft = 'zola',
      cmd = 'zola serve',
      dimensions = {
        height = 0.8,
        width = 0.5
      }
    })

    map("n", "<A-z>", function() zola_serve:toggle() end)
    map("t", "<A-z>", function() zola_serve:toggle() end)

    local python_repl = fterm:new({
      ft = 'fterm_python',
      cmd = 'python'
    })

    map("n", "<A-p>", function() python_repl:toggle() end)
    map("t", "<A-p>", function() python_repl:toggle() end)

    local gitui = fterm:new({
      ft = 'fterm_gitui',
      cmd = 'gitui',
      dimensions = {
        height = 0.9,
        width = 0.9
      }
    })

    map("n", "<A-g>", function() gitui:toggle() end)
    map("t", "<A-g>", function() gitui:toggle() end)

    local secondary = fterm:new({}) -- secondary terminal

    map("n", "<A-n>", function() secondary:toggle() end)
    map("t", "<A-n>", function() secondary:toggle() end)

    -- used for small processes like builds and such that only need to be run once
    map("n", "<A-t>", function()
      local args = fn.input("FTerm > ")
      local parts = vim.split(args, " ", { trimempty = true })
      -- expand any placeholders like %
      for i, part in ipairs(parts) do
        parts[i] = fn.expand(part)
      end
      args = table.concat(parts, " ")
      if args ~= '' then
        fterm.scratch({
          cmd = args,
          dimensions = {
            height = 0.8,
            width = 0.5
          }
        })
      end
    end)

  end
}
