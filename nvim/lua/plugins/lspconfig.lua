M = {
  "neovim/nvim-lspconfig",
  event = { "BufReadPost", "BufNewFile", "BufWritePre" },
}

M.config = function()
  -- LSP Configuration
  local lspconfig = require('lspconfig')
  local lsp = vim.lsp
  local diagnostic = vim.diagnostic
  local map = vim.keymap.set

  local set_qflist = function(buf_num, severity)
    local diagnostics = nil
    diagnostics = diagnostic.get(buf_num, { severity = severity })

    local qf_items = diagnostic.toqflist(diagnostics)
    vim.fn.setqflist({}, ' ', { title = 'Diagnostics', items = qf_items })

    -- open quickfix by default
    vim.cmd [[copen]]
  end

  -- additional capabilities supported by nvim-cmp
  local lsp_capabilities = require('cmp_nvim_lsp').default_capabilities()
  -- only map the following keys
  -- after the language server attaches to the current buffer
  local lsp_attach = function(client, bufnr)
    local opts = { buffer = bufnr }
    map('n', 'gD', lsp.buf.declaration, opts)
    map('n', 'gd', lsp.buf.definition, opts)
    map('n', 'K', lsp.buf.hover, opts)
    map('n', 'gi', lsp.buf.implementation, opts)
    map('n', '<C-k>', lsp.buf.signature_help, opts)
    map('n', '<space>wa', lsp.buf.add_workspace_folder, opts)
    map('n', '<space>wr', lsp.buf.remove_workspace_folder, opts)
    map('n', '<space>wl', function()
      print(vim.inspect(lsp.buf.list_workspace_folders()))
    end, opts)
    map('n', '<space>D', lsp.buf.type_definition, opts)
    map('n', '<space>rn', lsp.buf.rename, opts)
    map({ 'n', 'v' }, '<space>ca', lsp.buf.code_action, opts)
    map('n', 'gr', lsp.buf.references, opts)
    map('n', '<space>gl', diagnostic.open_float, opts)
    map('n', '[d', diagnostic.goto_prev, opts)
    map('n', ']d', diagnostic.goto_next, opts)
    -- this puts diagnostics from opened files to quickfix
    map('n', '<space>qw', diagnostic.setqflist, opts)
    -- this puts diagnostics from current buffer to quickfix
    map('n', '<space>qb', function() set_qflist(bufnr) end, opts)
  end

  local servers = require('mason-lspconfig').get_installed_servers()
  for _, server_name in ipairs(servers) do
    lspconfig[server_name].setup({
      on_attach = lsp_attach,
      capabilities = lsp_capabilities,
    })
  end

  lspconfig['denols'].setup({
    on_attach = lsp_attach,
    capabilities = lsp_capabilities,
    root_dir = lspconfig.util.root_pattern("deno.json", "deno.jsonc"),
  })

  lspconfig['ts_ls'].setup({
    on_attach = lsp_attach,
    capabilities = lsp_capabilities,
    root_dir = lspconfig.util.root_pattern("package.json"),
    single_file_support = false
  })

  -- fix undefined global 'vim'
  lspconfig['lua_ls'].setup({
    on_attach = lsp_attach,
    capabilities = lsp_capabilities,
    settings = {
      Lua = {
        diagnostics = {
          globals = { 'vim' }
        }
      }
    }
  })

  lspconfig['clangd'].setup({
    on_attach = lsp_attach,
    capabilities = lsp_capabilities,
    cmd = { "clangd", "--query-driver=/usr/bin/*g++*" },
  })

  lspconfig['gopls'].setup({
    on_attach = lsp_attach,
    capabilities = lsp_capabilities,
    settings = {
      gopls = {
        completeUnimported = true,
        usePlaceholders = true,
        analyses = {
          unusedparams = true,
        },
      },
    },
  })

  lsp.handlers["textDocument/hover"] = lsp.with(vim.lsp.handlers.hover, {
    border = "rounded",
  })
end

return M
