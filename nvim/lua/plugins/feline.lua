-- configure feline
local colors = {
  bg = "#24283b",
  bg_dark = "#1f2335",
  bg_highlight = "#292e42",
  blue = "#7aa2f7",
  blue0 = "#3d59a1",
  blue1 = "#2ac3de",
  blue2 = "#0db9d7",
  blue5 = "#89ddff",
  blue6 = "#b4f9f8",
  blue7 = "#394b70",
  comment = "#565f89",
  cyan = "#7dcfff",
  dark3 = "#545c7e",
  dark5 = "#737aa2",
  fg = "#c0caf5",
  fg_dark = "#a9b1d6",
  fg_gutter = "#3b4261",
  green = "#9ece6a",
  green1 = "#73daca",
  green2 = "#41a6b5",
  magenta = "#bb9af7",
  magenta2 = "#ff007c",
  orange = "#ff9e64",
  purple = "#9d7cd8",
  red = "#f7768e",
  red1 = "#db4b4b",
  teal = "#1abc9c",
  terminal_black = "#414868",
  yellow = "#e0af68",
  git = {
    add = "#449dab",
    change = "#6183bb",
    delete = "#914c54",
  },
}

colors.bg = 'NONE'
colors.error = colors.red1
colors.warning = colors.yellow
colors.info = colors.blue2
colors.hint = colors.teal

local function config(_, opts)
  local feline = require('feline')
  local vi_mode = require('feline.providers.vi_mode')
  local file = require('feline.providers.file')
  local git = require('feline.providers.git')
  local lsp = require('feline.providers.lsp')

  local empty = { fg = colors.bg, bg = colors.bg }

  local c = {
    vim_status = {
      provider = function()
        local s
        if require('lazy.status').has_updates() then
          s = require('lazy.status').updates()
        else
          s = '󰩖 '
        end
        s = string.format('%s', s)
        return s
      end,
      hl = function()
        return { fg = vi_mode.get_mode_color(), bg = colors.bg }
      end,
      left_sep = {
        always_visible = true,
        str = string.format('%s', ' '),
        hl = empty
      },
    },

    vi_mode = {
      provider = function()
        return string.format('%s', vim.api.nvim_get_mode().mode)
      end,
      hl = function()
        return { fg = vi_mode.get_mode_color(), bg = colors.bg }
      end,
      left_sep = {
        always_visible = true,
        str = string.format('%s', ' '),
        hl = empty
      },
    },

    file_name = {
      provider = {
        name = 'file_info',
        opts = {
          colored_icon = true,
          file_modified_icon = "",
          file_readonly_icon = "󰌾 ",
        },
      },
      hl = { fg = colors.magenta, bg = colors.bg },
      left_sep = {
        always_visible = true,
        str = string.format('%s', '  '),
        hl = empty
      },
    },

    git_branch = {
      provider = function()
        local branch, icon = git.git_branch()
        icon = '  '
        local s
        if #branch > 0 then
          s = string.format('%s%s', icon, branch)
        else
          s = string.format('%s', '')
        end
        return s
      end,
      hl = { fg = colors.orange, bg = colors.bg },
      right_sep = {
        always_visible = true,
        str = string.format('%s', '  '),
        hl = empty
      },
    },

    git_diff_added = {
      provider = function()
        if not git.git_info_exists() then
          return ''
        end
        local count, icon = git.git_diff_added()
        if count == '' then
          return ''
        end
        return string.format('%s%s', icon, count)
      end,
      hl = { fg = colors.git.add, bg = colors.bg },
      right_sep = {
        always_visible = false,
        str = string.format('%s', ' '),
        hl = empty
      },
    },

    git_diff_removed = {
      provider = function()
        if not git.git_info_exists() then
          return ''
        end
        local count, icon = git.git_diff_removed()
        if count == '' then
          return ''
        end
        return string.format('%s%s', icon, count)
      end,
      hl = { fg = colors.git.delete, bg = colors.bg },
      right_sep = {
        always_visible = false,
        str = string.format('%s', ' '),
        hl = empty
      },
    },

    git_diff_changed = {
      provider = function()
        if not git.git_info_exists() then
          return ''
        end
        local count, icon = git.git_diff_changed()
        if count == '' then
          return ''
        end
        return string.format('%s%s', icon, count)
      end,
      hl = { fg = colors.git.change, bg = colors.bg },
      right_sep = {
        always_visible = false,
        str = string.format('%s', ' '),
        hl = empty
      },
    },

    diagnostics_errors = {
      provider = function()
        if not lsp.is_lsp_attached() then
          return ''
        end
        local count, icon = lsp.diagnostic_errors()
        if count == '' then
          return ''
        end
        return string.format('%s%s', icon, count)
      end,
      hl = { fg = colors.error, bg = colors.bg },
      right_sep = {
        always_visible = false,
        str = string.format('%s', ' '),
        hl = empty
      },
    },

    diagnostics_warnings = {
      provider = function()
        if not lsp.is_lsp_attached() then
          return ''
        end
        local count, icon = lsp.diagnostic_warnings()
        if count == '' then
          return ''
        end
        return string.format('%s%s', icon, count)
      end,
      hl = { fg = colors.warning, bg = colors.bg },
      right_sep = {
        always_visible = false,
        str = string.format('%s', ' '),
        hl = empty
      },
    },

    diagnostics_info = {
      provider = function()
        if not lsp.is_lsp_attached() then
          return ''
        end
        local count, icon = lsp.diagnostic_info()
        if count == '' then
          return ''
        end
        return string.format('%s%s', icon, count)
      end,
      hl = { fg = colors.info, bg = colors.bg },
      right_sep = {
        always_visible = false,
        str = string.format('%s', ' '),
        hl = empty
      },
    },

    diagnostics_hints = {
      provider = function()
        if not lsp.is_lsp_attached() then
          return ''
        end
        local count, icon = lsp.diagnostic_hints()
        if count == '' then
          return ''
        end
        return string.format('%s%s', icon, count)
      end,
      hl = { fg = colors.hint, bg = colors.bg },
      right_sep = {
        always_visible = false,
        str = string.format('%s', ' '),
        hl = empty
      },
    },
    cursor_position = {
      provider = {
        name = 'position',
        opts = { padding = true },
      },
      hl = { fg = colors.fg, bg = colors.bg },
      right_sep = {
        always_visible = true,
        str = string.format('%s', ' '),
        hl = empty
      },
    },


    filler = {
      provider = ' ',
      hl = empty
    },
    -- inactive statusline
    in_file_info = {
      provider = function()
        if vim.api.nvim_buf_get_name(0) ~= '' then
          return file.file_info({}, {
            colored_icon = true,
            file_readonly_icon = "󰌾 ",
          })
        else
          return file.file_type(
            {},
            { colored_icon = true, case = 'lowercase' }
          )
        end
      end,
      hl = { fg = colors.magenta, bg = colors.bg },
    },
  }

  local active = {
    { -- left
      c.vim_status,
      c.vi_mode,
      c.file_name,
    },
    { -- right
      c.diagnostics_errors,
      c.diagnostics_warnings,
      c.diagnostics_info,
      c.diagnostics_hints,

      c.filler,

      c.git_diff_added,
      c.git_diff_removed,
      c.git_diff_changed,

      c.filler,

      c.git_branch,

      c.cursor_position,
    },
  }

  local inactive = {
    { -- left
      c.filler
    },
    { -- middle
      c.in_file_info,
    },
    { -- right
    },
  }

  opts.components = { active = active, inactive = inactive }

  feline.setup(opts)
end

return {
  'freddiehaddad/feline.nvim',
  config = config,
  opts = {
    force_inactive = {
      filetypes = {
        '^help$',
        '^FTerm$',
        '^mason$',
        '^lazy$',
        '^TelescopePrompt$',
        '^zola$',
        '^qf$',
        '^fterm_gitui$',
        '^fterm_python$',
      },
    },
    vi_mode_colors = {
      ['NORMAL'] = colors.red,
      ['OP'] = colors.green,
      ['CONFIRM'] = colors.orange,
      ['INSERT'] = colors.green,
      ['VISUAL'] = colors.blue,
      ['LINES'] = colors.blue,
      ['BLOCK'] = colors.blue,
      ['REPLACE'] = colors.violet,
      ['V-REPLACE'] = colors.violet,
      ['ENTER'] = colors.cyan,
      ['MORE'] = colors.cyan,
      ['SELECT'] = colors.orange,
      ['COMMAND'] = colors.green,
      ['SHELL'] = colors.green,
      ['TERM'] = colors.green,
      ['NONE'] = colors.yellow,
    },
  },
}
