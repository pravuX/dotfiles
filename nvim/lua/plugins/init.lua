return {
  {
    "nvim-lua/plenary.nvim",
  },

  "kyazdani42/nvim-web-devicons",

  {
    "norcalli/nvim-colorizer.lua",
    config = function()
      require("colorizer").setup {
        "*",
        -- Exclude vim from highlighting.
        "!help",
        "!TelescopePrompt",
        css = { rgb_fn = true, RRGGBBAA = true },
      }
    end,
    cmd = "ColorizerAttachToBuffer"
  },

  {
    "numToStr/Comment.nvim",
    config = function()
      require('Comment').setup()
    end,
    keys = "gc"
  },

  {
    "folke/which-key.nvim",
    event = "VeryLazy",
    opts = {
      win = {
        border = "rounded",
      },
    },
  },

  {
    "williamboman/mason-lspconfig.nvim",
    event = { "BufReadPost", "BufNewFile", "BufWritePre" },
    config = function()
      require('mason-lspconfig').setup({
        ensure_installed = {
          'rust_analyzer',
          'ts_ls',
          'eslint',
          'clangd',
          'lua_ls',
        }
      })
    end,
    dependencies = {
      {
        "williamboman/mason.nvim",
        config = function()
          require('mason').setup({
            ui = {
              border = "rounded"
            }
          })
        end
      },
    },
  },


}
