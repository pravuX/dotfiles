return {
  "lewis6991/gitsigns.nvim",
  event = "VeryLazy",
  config = function()
    require('gitsigns').setup {
      signs = {
        add          = { text = '┃' },
        change       = { text = '┃' },
        delete       = { text = '_' },
        topdelete    = { text = '‾' },
        changedelete = { text = '~' },
        untracked    = { text = '┇' },
      },
      watch_gitdir = {
        interval = 1000,
        follow_files = true
      },
      -- upadate interval for git signs
      update_debounce = 100,
    }
  end
}
