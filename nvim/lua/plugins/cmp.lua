return {
  "hrsh7th/nvim-cmp",
  event = "InsertEnter",
  dependencies = {
    "rafamadriz/friendly-snippets",
    {
      "L3MON4D3/LuaSnip",
      config = function()
        require("luasnip.loaders.from_vscode").lazy_load()
        local map = vim.keymap.set
        local opts = { silent = true }
        -- jumping placeholders in snippets
        map("i", "<c-j>", "<cmd>lua require'luasnip'.jump(1)<CR>", opts)
        map("s", "<c-j>", "<cmd>lua require'luasnip'.jump(1)<CR>", opts)
        map("i", "<c-k>", "<cmd>lua require'luasnip'.jump(-1)<CR>", opts)
        map("s", "<c-k>", "<cmd>lua require'luasnip'.jump(-1)<CR>", opts)
      end
    },
    "hrsh7th/cmp-buffer",
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-path",
    "hrsh7th/cmp-nvim-lua",
    "saadparwaiz1/cmp_luasnip",
    "onsails/lspkind-nvim",
  },
  config = function()
    local cmp = require 'cmp'
    local cmp_select = { behavior = cmp.SelectBehavior.Select }
    local lspkind = require('lspkind')
    local luasnip = require('luasnip')

    cmp.setup({

      snippet = {
        expand = function(args)
          luasnip.lsp_expand(args.body) -- luasnip.
        end,
      },

      window = {
        completion = cmp.config.window.bordered(),
        documentation = cmp.config.window.bordered()
      },

      formatting = {
        format = lspkind.cmp_format({
          mode = "symbol_text",
          menu = ({
            buffer = "[Buffer]",
            nvim_lsp = "[LSP]",
            luasnip = "[LuaSnip]",
            nvim_lua = "[Lua]",
            latex_symbols = "[Latex]",
          })
        })
      },

      mapping = {
        ['<C-b>'] = cmp.mapping.scroll_docs(-4),
        ['<C-f>'] = cmp.mapping.scroll_docs(4),
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<C-e>'] = cmp.mapping.abort(),
        ['<C-y>'] = cmp.mapping.confirm(),
        ['<CR>'] = cmp.mapping.confirm({ select = false }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
        ['<C-p>'] = cmp.mapping.select_prev_item(cmp_select),
        ['<C-n>'] = cmp.mapping.select_next_item(cmp_select),
      },

      sources = cmp.config.sources({
        { name = 'nvim_lsp' },
        { name = 'luasnip' },
        { name = 'buffer' },
        { name = 'nvim_lua' },
        { name = 'path' },
      })
    })
  end
}
