return {
  "nvim-telescope/telescope.nvim",
  keys = { "<leader>ff", "<leader>fs", "<leader>fg", "<leader>fb", "<leader>fh", "<C-p>", "<leader>en", "<leader>ep" },
  dependencies = {
    { "nvim-telescope/telescope-fzf-native.nvim", build = 'make' }
  },
  config = function()
    require('telescope').setup {
      extensions = {
        fzf = {}
      }
    }

    require('telescope').load_extension('fzf')


    local builtin = require('telescope.builtin')
    local map = vim.keymap.set
    map('n', '<leader>ff', builtin.find_files, {})
    map('n', '<C-p>', builtin.git_files, {})
    map('n', '<leader>fs', function()
      builtin.grep_string({ search = vim.fn.input("Grep > ") })
    end)
    map('n', '<leader>fg', builtin.live_grep, {})
    map('n', '<leader>fb', builtin.buffers, {})
    map('n', '<leader>fh', builtin.help_tags, {})

    map('n', '<leader>en', function()
      builtin.find_files {
        cwd = vim.fn.stdpath('config')
      }
    end)

    map('n', '<leader>ep', function()
      builtin.find_files {
        cwd = vim.fs.joinpath(vim.fn.stdpath('data'), 'lazy')
      }
    end)
  end
}
