return {
  "folke/tokyonight.nvim",
  config = function ()
  require("tokyonight").setup({
    style = "storm",
    light_style = "day",
    transparent = true,
    terminal_colors = true,
    styles = {
      comments = { italic = true },
      keywords = { italic = true },
      functions = { italic = true },
      variables = {},
      sidebars = "transparent",
      floats = "transparent",
    },
    hide_inactive_statusline = false,
    dim_inactive = false,

    on_highlights = function(hl, c)
      hl.WinSeparator = {
        bold = true,
        fg = c.dark5,
      }
      hl.LineNr = {
        fg = c.dark5,
      }
      hl.CursorLineNr = {
        fg = c.fg
      }
      hl.CursorLine = {
        bg = c.none
      }
      hl.NullLsInfoBorder = hl.FloatBorder
    end,
  })

  vim.cmd [[colorscheme tokyonight]]
  end
}
