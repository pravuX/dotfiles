require("pravuX.remap")
require("pravuX.set")
require("pravuX.lazy")

local augroup = vim.api.nvim_create_augroup
local pravux_group = augroup('pravuX', {})
local terminal_group = augroup('Terminal', {})

local autocmd = vim.api.nvim_create_autocmd
local yank_group = augroup('HighlightYank', { clear = true })

function R(name)
  require("plenary.reload").reload_module(name)
end

-- highlight on yank
autocmd('TextYankPost', {
  group = yank_group,
  pattern = '*',
  callback = function()
    vim.highlight.on_yank({
      higroup = 'IncSearch',
      timeout = 40,
    })
  end,
})

-- autocommands
-- remove trailing whitespace on write

autocmd({ "BufWritePre" }, {
  group = pravux_group,
  pattern = "*",
  command = [[%s/\s\+$//e]],
})

autocmd({ "BufWritePre" }, {
  group = pravux_group,
  pattern = "*",
  command = [[%s/\n\+\%$//e]],
})

-- prevent autocommenting new line
autocmd({ "BufEnter" }, {
  group = pravux_group,
  pattern = "*",
  command = [[set fo-=cro]]
})

autocmd({ "TermOpen" }, {
  group = terminal_group,
  pattern = "*",
  command = "startinsert"
})

autocmd({ "TermOpen" }, {
  group = terminal_group,
  pattern = "*",
  command = "setlocal listchars= nonumber norelativenumber"
})

vim.g.netrw_browse_split = 0
vim.g.netrw_banner = 0
vim.g.netrw_winsize = 25
