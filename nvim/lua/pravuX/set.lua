local opt = vim.opt
local fn = vim.fn

-- get a nice numberline
opt.nu = true
opt.relativenumber = true

-- sane splitting
opt.splitbelow = true
opt.splitright = true

-- indentation
opt.tabstop = 2
opt.softtabstop = 2
opt.shiftwidth = 2
opt.smartindent = true
opt.expandtab = true

-- speed up ESC squence
opt.ttimeoutlen = 0
opt.timeoutlen  = 500

-- display whitespace
opt.showbreak = '¬'
opt.list = true
opt.lcs = [[tab: ,space:·,eol:↴]]

-- undo
opt.swapfile = false
opt.backup = false
opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
opt.undofile = true

-- intelligent searching
opt.hlsearch = false
opt.incsearch = true
opt.smartcase = true

-- use os clipboard
opt.clipboard = "unnamedplus"

-- miscellaneous
opt.termguicolors = true
opt.wrap = false
opt.cursorline = true
opt.scrolloff = 8
opt.signcolumn = "yes"
opt.isfname:append("@-@")
opt.updatetime = 50
opt.colorcolumn = ""
opt.laststatus = 3
opt.showmode = false
opt.hidden = true
opt.linebreak = true
opt.mousemodel = "extend"

-- completion
opt.completeopt = 'menu,menuone,noselect'

-- set diagnostic icons
local signs = { Error = "", Warn = "", Hint = "", Info = "" }

for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
end
