local map = vim.keymap.set

vim.g.mapleader = " "

-- open netrw
map("n", "<leader>nw", vim.cmd.Ex)

-- move blocks of code
map("v", "J", ":m '>+1<CR>gv=gv")
map("v", "K", ":m '<-2<CR>gv=gv")

-- place cursor at start while J
map("n", "J", "mzJ`z")

-- center the cursor always
map("n", "<C-d>", "<C-d>zz")
map("n", "<C-u>", "<C-u>zz")
map("n", "n", "nzzzv")
map("n", "N", "Nzzzv")

-- copy to the end of the line
map('n', 'vv', 'v$')

-- copy to void register while pasting over or deleting
map("x", "<leader>p", [["_dP]])
map({ "n", "v" }, "<leader>d", [["_d]])

map("n", "Q", "<nop>")

-- search word under cursor
map("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
-- make open script executable
map("n", "<leader>x", "<cmd>!chmod +x %<CR>", { silent = true })

-- change size of split panes
map('n', '<M-H>', '<C-w><')
map('n', '<M-L>', '<C-w>>')
map('n', '<M-J>', '<C-w>-')
map('n', '<M-K>', '<C-w>+')
map('t', '<M-H>', [[<C-\><C-n><C-w><]])
map('t', '<M-L>', [[<C-\><C-n><C-w>>]])
map('t', '<M-J>', [[<C-\><C-n><C-w>-]])
map('t', '<M-K>', [[<C-\><C-n><C-w>+]])

-- switch between split screens
map('', '<M-h>', '<C-w><C-h>')
map('', '<M-l>', '<C-w><C-l>')
map('', '<M-j>', '<C-w><C-j>')
map('', '<M-k>', '<C-w><C-k>')
map('t', '<Esc>', [[<C-\><C-n>]])
map('t', '<M-h>', [[<C-\><C-n><C-w>h]])
map('t', '<M-l>', [[<C-\><C-n><C-w>l]])
map('t', '<M-j>', [[<C-\><C-n><C-w>j]])
map('t', '<M-k>', [[<C-\><C-n><C-w>k]])
