# dotfiles
## using
- Fonts: Input Mono, Hack Nerd Font Mono, Symbols Nerd Font, Terminus, Julia Mono, JoyPixels, Victor Mono
- Terminal: [foot](https://codeberg.org/dnkl/foot)
- WM: sway
- Editor: nvim
- Statusbar: waybar
- menu/launcher: bemenu, [rofi with wayland support](https://github.com/lbonn/rofi)

## usage
simply create respective symlinks:

`ln -s [path/to/source] [shortcut]`
