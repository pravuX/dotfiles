#
# ~/.bash_profile
#

if [[ $- == *i* ]]; then . ~/.bashrc; fi

PATH=$PATH:~/.local/bin:~/.bin:~/.npm-global/bin:~/.cargo/bin:~/.local/go/bin:~/go/bin:~/.deno/bin:~/zig
