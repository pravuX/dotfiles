#
# ~/.bash_aliases
#

# functions for aliases
# function printStats {
#     awk -F, 'BEGIN {print "╭────────────────╮\n│Pomodoro History│\n├───────────┬────┤"} /[0-9]+/ {printf "│%s │%4s│\n", $1, $2} END {print "╰───────────┴────╯"}' ~/scripts/pomodoro/data.csv
# }
function webm2gif {
  for file in "$@"; do
    echo "Converting $file..."
    ffmpeg -y -i "$file" -vf "fps=15,scale=640:-1:flags=lanczos" -c:v gif "${file%.webm}.gif"

    echo "Compressing ${file%.webm}.gif..."
    gifsicle -b -O2 "${file%.webm}.gif"

    echo "${file%.webm}.gif created and compressed!"
  done
  echo "All done!"
}

# old habits, new programs
alias htop=btop
alias grep=rg
alias find=fdfind
alias ls=lsd

# handy aliases
alias l='lsd'
alias ll='lsd -al'
alias la='lsd -a'
alias serve="browser-sync -s -f ."
alias prime-run="__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia"
alias vulk-run="__NV_PRIME_RENDER_OFFLOAD=1"
alias png2gif="convert -delay 200 -loop 0 *.png output.gif"

alias hx='helix'
alias ride=~/Downloads/Dyalog/opt/ride-4.4/Ride-4.4
alias bqn='rlwrap ~/CBQN/BQN'
# alias pomodoro=~/scripts/pomodoro/pomodoro.py
# alias po25='pomodoro 25+5'
# alias po55='pomodoro 55+5'
# alias show_history=printStats
alias show_active_connection='nmcli c show --active | cut -d " " -f 1 | sed -n 2p'
alias whatis_eating_my_storage="du -aBM -d 1 | sort -nr | grep -e '.*\d+(M|G)'"

# fix common mixtakes
alias ivm=vim
alias mvi=vim
alias dc=cd
alias sl=ls

# git aliases
alias gis='git status'
alias gia='git add'
alias gic='git commit -m'
alias gip='git push'
alias gipl='git pull'

alias sway='env __GL_GSYNC_ALLOWED=0 __GL_VRR_ALLOWED=0 WLR_DRM_NO_ATOMIC=1 QT_AUTO_SCREEN_SCALE_FACTOR=1 QT_QPA_PLATFORM=wayland QT_WAYLAND_DISABLE_WINDOWDECORATION=1 GTK_USE_PORTAL=0 GDK_BACKEND=wayland XDG_CURRENT_DESKTOP=sway GBM_BACKEND=nvidia-drm __GLX_VENDOR_LIBRARY_NAME=nvidia WLR_NO_HARDWARE_CURSORS=1 sway --unsupported-gpu'
