local wezterm = require 'wezterm'

local config = {}

if wezterm.config_builder then
  config = wezterm.config_builder()
end

config.default_prog = { '/usr/bin/bash' }
config.audible_bell = 'Disabled'
config.adjust_window_size_when_changing_font_size = false
config.window_decorations = "RESIZE"
config.enable_scroll_bar = false
config.color_scheme = 'Tokyo Night Storm'
config.enable_tab_bar = false

config.window_background_opacity = 0.90
config.window_padding = {
  left = 2,
  right = 0,
  top = 0,
  bottom = 0,
}

config.font_size = 14.0
config.font = wezterm.font_with_fallback {
  'JetBrains Mono',
  { family = "Symbols Nerd Font Mono", scale = 0.80 },
}

config.font_rules = {
  {
    intensity = 'Bold',
    italic = true,
    font = wezterm.font {
      family = 'VictorMono',
      weight = 'ExtraBold',
      style = 'Italic'
    },
  },
  {
    italic = true,
    intensity = 'Half',
    font = wezterm.font {
      family = 'VictorMono',
      weight = 'Bold',
      style = 'Italic',
    },
  },
  {
    italic = true,
    intensity = 'Normal',
    font = wezterm.font {
      family = 'VictorMono',
      weight = 'DemiBold',
      style = 'Italic',
    },
  },
}

return config
